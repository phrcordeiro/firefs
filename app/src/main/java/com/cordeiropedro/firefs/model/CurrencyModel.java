package com.cordeiropedro.firefs.model;

public class CurrencyModel {

    String currency;
    double rate;

    public CurrencyModel(String currency, double rate) {
        this.currency = currency;
        this.rate = rate;
    }

    public Double calculateAmount(double value){
        return value * rate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
