package com.cordeiropedro.firefs.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RateModel implements Serializable {

    double EUR;
    double AUD;
    double BGN;
    double BRL;
    double CAD;
    double CHF;
    double CNY;
    double CZK;
    double DKK;
    double GBP;
    double HKD;
    double HRK;
    double HUF;
    double IDR;
    double ILS;
    double INR;
    double JPY;
    double KRW;
    double MXN;
    double MYR;
    double NOK;
    double NZD;
    double PHP;
    double PLN;
    double RON;
    double RUB;
    double SEK;
    double SGD;
    double THB;
    double TRY;
    double USD;
    double ZAR;

    public List<CurrencyModel> convertToList(){
        List<CurrencyModel> currencyModels = new ArrayList<>();
        currencyModels.add(new CurrencyModel("EUR", EUR));
        currencyModels.add(new CurrencyModel("AUD", AUD));
        currencyModels.add(new CurrencyModel("BGN", BGN));
        currencyModels.add(new CurrencyModel("BRL", BRL));
        currencyModels.add(new CurrencyModel("CAD", CAD));
        currencyModels.add(new CurrencyModel("CHF", CHF));
        currencyModels.add(new CurrencyModel("CNY", CNY));
        currencyModels.add(new CurrencyModel("CZK", CZK));
        currencyModels.add(new CurrencyModel("DKK", DKK));
        currencyModels.add(new CurrencyModel("GBP", GBP));
        currencyModels.add(new CurrencyModel("HKD", HKD));
        currencyModels.add(new CurrencyModel("HRK", HRK));
        currencyModels.add(new CurrencyModel("HUF", HUF));
        currencyModels.add(new CurrencyModel("IDR", IDR));
        currencyModels.add(new CurrencyModel("ILS", ILS));
        currencyModels.add(new CurrencyModel("INR", INR));
        currencyModels.add(new CurrencyModel("JPY", JPY));
        currencyModels.add(new CurrencyModel("KRW", KRW));
        currencyModels.add(new CurrencyModel("MXN", MXN));
        currencyModels.add(new CurrencyModel("MYR", MYR));
        currencyModels.add(new CurrencyModel("NOK", NOK));
        currencyModels.add(new CurrencyModel("NZD", NZD));
        currencyModels.add(new CurrencyModel("PHP", PHP));
        currencyModels.add(new CurrencyModel("PLN", PLN));
        currencyModels.add(new CurrencyModel("RON", RON));
        currencyModels.add(new CurrencyModel("RUB", RUB));
        currencyModels.add(new CurrencyModel("SEK", SEK));
        currencyModels.add(new CurrencyModel("SGD", SGD));
        currencyModels.add(new CurrencyModel("THB", THB));
        currencyModels.add(new CurrencyModel("TRY", TRY));
        currencyModels.add(new CurrencyModel("USD", USD));
        currencyModels.add(new CurrencyModel("ZAR", ZAR));
        return currencyModels;
    }

    public Map<String, CurrencyModel> convertToMap(){
        Map<String, CurrencyModel> currencyModels = new HashMap<>();
        currencyModels.put("EUR", new CurrencyModel("EUR", EUR));
        currencyModels.put("AUD", new CurrencyModel("AUD", AUD));
        currencyModels.put("BGN", new CurrencyModel("BGN", BGN));
        currencyModels.put("BRL", new CurrencyModel("BRL", BRL));
        currencyModels.put("CAD", new CurrencyModel("CAD", CAD));
        currencyModels.put("CHF", new CurrencyModel("CHF", CHF));
        currencyModels.put("CNY", new CurrencyModel("CNY", CNY));
        currencyModels.put("CZK", new CurrencyModel("CZK", CZK));
        currencyModels.put("DKK", new CurrencyModel("DKK", DKK));
        currencyModels.put("GBP", new CurrencyModel("GBP", GBP));
        currencyModels.put("HKD", new CurrencyModel("HKD", HKD));
        currencyModels.put("HRK", new CurrencyModel("HRK", HRK));
        currencyModels.put("HUF", new CurrencyModel("HUF", HUF));
        currencyModels.put("IDR", new CurrencyModel("IDR", IDR));
        currencyModels.put("ILS", new CurrencyModel("ILS", ILS));
        currencyModels.put("INR", new CurrencyModel("INR", INR));
        currencyModels.put("JPY", new CurrencyModel("JPY", JPY));
        currencyModels.put("KRW", new CurrencyModel("KRW", KRW));
        currencyModels.put("MXN", new CurrencyModel("MXN", MXN));
        currencyModels.put("MYR", new CurrencyModel("MYR", MYR));
        currencyModels.put("NOK", new CurrencyModel("NOK", NOK));
        currencyModels.put("NZD", new CurrencyModel("NZD", NZD));
        currencyModels.put("PHP", new CurrencyModel("PHP", PHP));
        currencyModels.put("PLN", new CurrencyModel("PLN", PLN));
        currencyModels.put("RON", new CurrencyModel("RON", RON));
        currencyModels.put("RUB", new CurrencyModel("RUB", RUB));
        currencyModels.put("SEK", new CurrencyModel("SEK", SEK));
        currencyModels.put("SGD", new CurrencyModel("SGD", SGD));
        currencyModels.put("THB", new CurrencyModel("THB", THB));
        currencyModels.put("TRY", new CurrencyModel("TRY", TRY));
        currencyModels.put("USD", new CurrencyModel("USD", USD));
        currencyModels.put("ZAR", new CurrencyModel("ZAR", ZAR));
        return currencyModels;
    }

    public double getAUD() {
        return AUD;
    }

    public void setAUD(double AUD) {
        this.AUD = AUD;
    }

    public double getBGN() {
        return BGN;
    }

    public void setBGN(double BGN) {
        this.BGN = BGN;
    }

    public double getBRL() {
        return BRL;
    }

    public void setBRL(double BRL) {
        this.BRL = BRL;
    }

    public double getCAD() {
        return CAD;
    }

    public void setCAD(double CAD) {
        this.CAD = CAD;
    }

    public double getCHF() {
        return CHF;
    }

    public void setCHF(double CHF) {
        this.CHF = CHF;
    }

    public double getCNY() {
        return CNY;
    }

    public void setCNY(double CNY) {
        this.CNY = CNY;
    }

    public double getCZK() {
        return CZK;
    }

    public void setCZK(double CZK) {
        this.CZK = CZK;
    }

    public double getDKK() {
        return DKK;
    }

    public void setDKK(double DKK) {
        this.DKK = DKK;
    }

    public double getGBP() {
        return GBP;
    }

    public void setGBP(double GBP) {
        this.GBP = GBP;
    }

    public double getHKD() {
        return HKD;
    }

    public void setHKD(double HKD) {
        this.HKD = HKD;
    }

    public double getHRK() {
        return HRK;
    }

    public void setHRK(double HRK) {
        this.HRK = HRK;
    }

    public double getHUF() {
        return HUF;
    }

    public void setHUF(double HUF) {
        this.HUF = HUF;
    }

    public double getIDR() {
        return IDR;
    }

    public void setIDR(double IDR) {
        this.IDR = IDR;
    }

    public double getILS() {
        return ILS;
    }

    public void setILS(double ILS) {
        this.ILS = ILS;
    }

    public double getINR() {
        return INR;
    }

    public void setINR(double INR) {
        this.INR = INR;
    }

    public double getJPY() {
        return JPY;
    }

    public void setJPY(double JPY) {
        this.JPY = JPY;
    }

    public double getKRW() {
        return KRW;
    }

    public void setKRW(double KRW) {
        this.KRW = KRW;
    }

    public double getMXN() {
        return MXN;
    }

    public void setMXN(double MXN) {
        this.MXN = MXN;
    }

    public double getMYR() {
        return MYR;
    }

    public void setMYR(double MYR) {
        this.MYR = MYR;
    }

    public double getNOK() {
        return NOK;
    }

    public void setNOK(double NOK) {
        this.NOK = NOK;
    }

    public double getNZD() {
        return NZD;
    }

    public void setNZD(double NZD) {
        this.NZD = NZD;
    }

    public double getPHP() {
        return PHP;
    }

    public void setPHP(double PHP) {
        this.PHP = PHP;
    }

    public double getPLN() {
        return PLN;
    }

    public void setPLN(double PLN) {
        this.PLN = PLN;
    }

    public double getRON() {
        return RON;
    }

    public void setRON(double RON) {
        this.RON = RON;
    }

    public double getRUB() {
        return RUB;
    }

    public void setRUB(double RUB) {
        this.RUB = RUB;
    }

    public double getSEK() {
        return SEK;
    }

    public void setSEK(double SEK) {
        this.SEK = SEK;
    }

    public double getSGD() {
        return SGD;
    }

    public void setSGD(double SGD) {
        this.SGD = SGD;
    }

    public double getTHB() {
        return THB;
    }

    public void setTHB(double THB) {
        this.THB = THB;
    }

    public double getTRY() {
        return TRY;
    }

    public void setTRY(double TRY) {
        this.TRY = TRY;
    }

    public double getUSD() {
        return USD;
    }

    public void setUSD(double USD) {
        this.USD = USD;
    }

    public double getZAR() {
        return ZAR;
    }

    public void setZAR(double ZAR) {
        this.ZAR = ZAR;
    }
}
