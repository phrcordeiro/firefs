package com.cordeiropedro.firefs.model.enuns;

import com.cordeiropedro.firefs.R;

public enum MenuDrawerItem {

    HOME(R.id.menu_drawer_home),
    CONSULT (R.id.menu_drawer_consult);

    protected int menuId;

    MenuDrawerItem(int menuId){
        this.menuId = menuId;
    }

    public static MenuDrawerItem findById(int menuId){
        MenuDrawerItem[] menu = MenuDrawerItem.values();
        for (int i = 0; i < menu.length; i++) {
            if (menuId == menu[i].menuId) {
                return menu[i];
            }
        }
        return HOME;
    }
}
