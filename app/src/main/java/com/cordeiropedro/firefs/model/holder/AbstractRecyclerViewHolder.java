package com.cordeiropedro.firefs.model.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public class AbstractRecyclerViewHolder extends RecyclerView.ViewHolder {
    public AbstractRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
