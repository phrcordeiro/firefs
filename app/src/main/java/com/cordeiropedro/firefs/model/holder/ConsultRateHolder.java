package com.cordeiropedro.firefs.model.holder;

import android.view.View;
import android.widget.TextView;

import com.cordeiropedro.firefs.R;

import butterknife.BindView;


/**
 * Created by phrc on 05/07/16.
 */

public class ConsultRateHolder extends AbstractRecyclerViewHolder {

    @BindView(R.id.currency)
    TextView currency;
    @BindView(R.id.rate)
    TextView rate;

    public ConsultRateHolder(View itemView) {
        super(itemView);
    }

    public TextView getCurrency() {
        return currency;
    }

    public void setCurrency(TextView currency) {
        this.currency = currency;
    }

    public TextView getRate() {
        return rate;
    }

    public void setRate(TextView rate) {
        this.rate = rate;
    }
}
