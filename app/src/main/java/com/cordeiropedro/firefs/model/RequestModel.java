package com.cordeiropedro.firefs.model;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pedro on 8/19/14.
 */
public class RequestModel implements Serializable {

    String base;
    String date;
    RateModel rates;

    public RequestModel() {
    }

    public static RequestModel getInstance(JSONObject jsonObject) {
        if (jsonObject != null) {
            RequestModel requestModel =
                    new Gson().fromJson(jsonObject.toString(), RequestModel.class);
            return requestModel;
        }
        return null;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public RateModel getRates() {
        return rates;
    }

    public void setRates(RateModel rates) {
        this.rates = rates;
    }
}
