package com.cordeiropedro.firefs.view.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import com.cordeiropedro.firefs.R;
import com.cordeiropedro.firefs.model.enuns.MenuDrawerItem;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.fragments_container)
    FrameLayout fragmentContainer;
    @BindView(R.id.menu_drawer)
    DrawerLayout menuDrawer;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    protected ActionBarDrawerToggle mDrawerToggle;

    AbstractMenuDrawerActivity menuDrawerActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setUpComponentes();
        setSupportActionBar(toolbar);
        mDrawerToggle = new ActionBarDrawerToggle(this, menuDrawer, toolbar, R.string.app_name, R.string.app_name);
        menuDrawerActivity.swicthFragment(MenuDrawerItem.HOME);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    private void setUpComponentes() {
        menuDrawerActivity = new AbstractMenuDrawerActivity() {
            @Override
            protected NavigationView getNavigationView() {
                return navigationView;
            }

            @Override
            protected DrawerLayout getToDrawerLayout() {
                return menuDrawer;
            }

            @Override
            protected void onSwicthFragment(MenuDrawerItem frag) {}

            @Override
            protected int getToIdFragment() {
                return R.id.fragments_container;
            }

            @Override
            protected FragmentManager getFragmentManager() {
                return getSupportFragmentManager();
            }

        };
    }
}
