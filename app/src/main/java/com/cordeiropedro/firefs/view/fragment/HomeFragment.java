package com.cordeiropedro.firefs.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cordeiropedro.firefs.R;
import com.cordeiropedro.firefs.controller.connection.listerners.RateListener;
import com.cordeiropedro.firefs.controller.connection.requests.RateRequester;
import com.cordeiropedro.firefs.controller.connection.transformer.RateTransformer;
import com.cordeiropedro.firefs.model.CurrencyModel;
import com.cordeiropedro.firefs.model.RequestModel;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

public class HomeFragment extends Fragment {

    @BindView(R.id.list_base_currency)
    AppCompatSpinner baseCurrencySpinner;
    @BindView(R.id.list_currency)
    AppCompatSpinner currencySpinner;
    @BindView(R.id.input_amount)
    EditText amount;
    @BindView(R.id.calculate)
    Button calculate;
    @BindView(R.id.value)
    TextView value;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;


    RateRequester rateRequester;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        rateRequester = new RateRequester(getContext());

        spinnerSetup();
        value.setVisibility(View.GONE);
        return view;
    }

    private void spinnerSetup() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.currency, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currencySpinner.setAdapter(adapter);
        currencySpinner.setSelection(1);
        baseCurrencySpinner.setAdapter(adapter);
        baseCurrencySpinner.setSelection(0);
    }

    @OnClick(R.id.calculate) public void setCalculateClick(){
        progressON();
        rateRequester.getRate(baseCurrencySpinner.getSelectedItem().toString(),
                currencySpinner.getSelectedItem().toString(),
                new RateTransformer(new RateListener() {
                    @Override
                    public void onNetworkError(int statusCode,
                                               Header[] headers,
                                               byte[] responseBody,
                                               Throwable error) {
                        super.onNetworkError(statusCode, headers, responseBody, error);
                        value.setVisibility(View.GONE);
                        progressOFF();
                    }

                    @Override
                    public void onSucess(RequestModel content, Header[] headers) {
                        super.onSucess(content, headers);
                        HashMap<String, CurrencyModel> currencyModels =
                                (HashMap<String, CurrencyModel>) content.getRates().convertToMap();
                        CurrencyModel currencyModel = currencyModels.get(
                                currencySpinner.getSelectedItem().toString());
                        value.setText(
                                String.valueOf(currencyModel.calculateAmount(
                                        Double.valueOf(amount.getText().toString()))));
                        value.setVisibility(View.VISIBLE);
                        progressOFF();
                    }
                }));
    }

    private void progressON(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void progressOFF(){
        progressBar.setVisibility(View.GONE);
    }

}
