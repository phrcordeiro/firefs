package com.cordeiropedro.firefs.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.cordeiropedro.firefs.R;
import com.cordeiropedro.firefs.model.CurrencyModel;
import com.cordeiropedro.firefs.model.holder.ConsultRateHolder;

import java.util.List;

public class ConsultRaterAdapter extends FasterCardAdapter<CurrencyModel> {

    public ConsultRaterAdapter(List<CurrencyModel> listObjects, Context context) {
        super(listObjects, context);
    }

    @Override
    protected int layoutResourceForItem(int position) {
        return R.layout.item_consult_rate;
    }

    @Override
    protected void fillHolder(RecyclerView.ViewHolder abstractHolder, int position) {
        ConsultRateHolder holder = (ConsultRateHolder) abstractHolder;
        CurrencyModel model = listObjects.get(position);
        holder.getCurrency().setText(model.getCurrency());
        holder.getRate().setText(String.valueOf(model.getRate()));
    }

    @Override
    protected RecyclerView.ViewHolder inflateView(View view, int position) {
        return new ConsultRateHolder(view);
    }
}
