package com.cordeiropedro.firefs.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cordeiropedro.firefs.R;
import com.cordeiropedro.firefs.controller.connection.listerners.RateListener;
import com.cordeiropedro.firefs.controller.connection.requests.RateRequester;
import com.cordeiropedro.firefs.controller.connection.transformer.RateTransformer;
import com.cordeiropedro.firefs.model.CurrencyModel;
import com.cordeiropedro.firefs.model.RequestModel;
import com.cordeiropedro.firefs.view.adapter.ConsultRaterAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

public class ConsultRateFragment extends Fragment {

    @BindView(R.id.list_currency)
    AppCompatSpinner currencySpinner;
    @BindView(R.id.list_rate)
    RecyclerView mRecyclerView;
    @BindView(R.id.datepicker)
    TextView date;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    ConsultRaterAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    List<CurrencyModel> rateModels;
    RateRequester rateRequester;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_consult_rate, container, false);
        ButterKnife.bind(this, view);

        currencyListSetup();
        baseCurrencySpinnerSetup();
        setInitDate();
        progressBar.setVisibility(View.GONE);
        rateRequester = new RateRequester(null);
        return view;
    }

    @OnClick(R.id.datepicker)
    public void setDatePickerClick() {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.setDateSetListern(new IDatePickerListern() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                String month = String.valueOf((i1 < 10) ? "0" + (i1 + 1) : (i1 + 1));
                String day = String.valueOf((i2 < 10) ? "0" + i2 : i2);
                date.setText(i + "-" + month + "-" + day);
                updateList();
            }
        });
        newFragment.show(getFragmentManager(), "date");
    }

    private void currencyListSetup() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        rateModels = new ArrayList<>();
        mAdapter = new ConsultRaterAdapter(rateModels, getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

    private void baseCurrencySpinnerSetup() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.currency, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currencySpinner.setAdapter(adapter);
        currencySpinner.setSelection(0);
        currencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                updateList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setInitDate() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int intMonth = c.get(Calendar.MONTH);
        int intDay = c.get(Calendar.DAY_OF_MONTH);
        String month = String.valueOf((intMonth < 10) ? "0" + (intMonth + 1) : (intMonth + 1));
        String day = String.valueOf((intDay < 10) ? "0" + intDay : intDay);
        date.setText(year + "-" + month + "-" + day);
    }

    private void updateList() {
        if (date != null &&
                currencySpinner != null &&
                !date.getText().equals("")) {

            progressON();
            rateRequester.getRateByDate(currencySpinner.getSelectedItem().toString(),
                    date.getText().toString(),
                    new RateTransformer(new RateListener() {
                        @Override
                        public void onSucess(RequestModel content, Header[] headers) {
                            super.onSucess(content, headers);
                            fillRateList(content);
                            progressOFF();
                        }

                        @Override
                        public void onNetworkError(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            super.onNetworkError(statusCode, headers, responseBody, error);
                            rateModels.clear();
                            mAdapter.notifyDataSetChanged();
                            progressOFF();
                        }
                    }));
        }
    }

    public void fillRateList(RequestModel content) {
        rateModels.clear();
        rateModels.addAll(content.getRates().convertToList());
        mAdapter.notifyDataSetChanged();
    }

    private void progressON(){
        progressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
    }

    private void progressOFF(){
        progressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

}
