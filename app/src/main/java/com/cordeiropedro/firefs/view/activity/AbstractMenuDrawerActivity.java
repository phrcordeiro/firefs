package com.cordeiropedro.firefs.view.activity;

import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.MenuItem;

import com.cordeiropedro.firefs.model.enuns.MenuDrawerItem;
import com.cordeiropedro.firefs.view.fragment.ConsultRateFragment;
import com.cordeiropedro.firefs.view.fragment.HomeFragment;

/**
 * Created by pedro on 11/25/14.
 */
public abstract class AbstractMenuDrawerActivity {

    public static final String TAG_HOME = "home";
    public static final String TAG_CONSULT_RATE = "consult_rate";


    protected String currentFragment;

    protected abstract NavigationView getNavigationView();

    protected abstract DrawerLayout getToDrawerLayout();

    protected abstract void onSwicthFragment(MenuDrawerItem frag);

    protected abstract int getToIdFragment();

    protected abstract FragmentManager getFragmentManager();

    public AbstractMenuDrawerActivity() {
        getNavigationView().setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                swicthFragment(MenuDrawerItem.findById(menuItem.getItemId()));
                return true;
            }
        });
    }

    // --------------------------------------------------------------------------------------------------------------------------
    // Fragment Management
    // --------------------------------------------------------------------------------------------------------------------------

    protected void startFragment(Fragment fragment, String tag) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        loadCurrentFragment(fragment, ft, tag);
    }

    protected void loadCurrentFragment(Fragment fragment, FragmentTransaction ft, String tag) {
        ft.replace(getToIdFragment(), fragment, tag).commit();
    }

    public void swicthFragment(MenuDrawerItem frag) {
        switch (frag) {
            case HOME:
                currentFragment = TAG_HOME;
                startFragment(new HomeFragment(), TAG_HOME);
                break;
            case CONSULT:
                currentFragment = TAG_CONSULT_RATE;
                startFragment(new ConsultRateFragment(), TAG_CONSULT_RATE);
                break;
            default:
                break;
        }
        onSwicthFragment(frag);
        getToDrawerLayout().closeDrawer(Gravity.LEFT);
    }

    protected String getCurrentFragment() {
        return currentFragment;
    }

}
