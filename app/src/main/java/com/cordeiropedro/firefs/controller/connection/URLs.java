package com.cordeiropedro.firefs.controller.connection;


public class URLs {

    public static String URL_LATEST = WSConfig.BASE_URL+"latest";

    public static String getRateList(String date){
        return WSConfig.BASE_URL+date;
    }

}
