package com.cordeiropedro.firefs.controller.connection.listerners;

import com.cordeiropedro.firefs.model.RequestModel;
import cz.msebera.android.httpclient.Header;

public abstract class RateListener implements IListeners {

    @Override
    public void onNetworkError(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

    }

    public void onSucess(RequestModel content, Header[] headers) {

    }

}
