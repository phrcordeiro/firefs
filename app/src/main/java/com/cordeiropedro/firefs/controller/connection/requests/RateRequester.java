package com.cordeiropedro.firefs.controller.connection.requests;

import android.content.Context;

import com.cordeiropedro.firefs.controller.connection.URLs;
import com.cordeiropedro.firefs.controller.connection.transformer.RateTransformer;

import java.util.HashMap;
import java.util.Map;

public class RateRequester extends AbstractRequester {

    public RateRequester(Context context) {
        super(context);
    }

    public void getRateByDate(String base, String date, RateTransformer transformer){
        controller.get(context, URLs.getRateList(date), makeRequestParamsWith("base", base), transformer);
    }

    public void getRate(String base, String symbol, RateTransformer transformer){
        Map<String, String> params = new HashMap<>();
        params.put("base", base);
        params.put("symbols", symbol);
        controller.get(context, URLs.URL_LATEST, makeRequestParamsWith(params), transformer);
    }
}
