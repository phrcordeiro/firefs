package com.cordeiropedro.firefs.controller.connection.listerners;

import com.cordeiropedro.firefs.model.RequestModel;
import cz.msebera.android.httpclient.Header;


public interface IListeners {

    public void onNetworkError(int statusCode,
                               Header[] headers,
                               byte[] responseBody,
                               Throwable error);

}
