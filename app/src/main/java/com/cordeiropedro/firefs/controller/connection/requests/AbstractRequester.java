package com.cordeiropedro.firefs.controller.connection.requests;

import android.content.Context;

import com.cordeiropedro.firefs.controller.connection.ConnectionController;
import com.loopj.android.http.RequestParams;

import java.io.Serializable;
import java.util.Map;

public abstract class AbstractRequester implements Serializable {

    protected Context context;
    protected ConnectionController controller;

    public AbstractRequester(Context context) {
        this.context = context;
        controller = ConnectionController.getInstance();
    }

    //----------------------------------------------------------------------------------------------
    // Request Params
    //----------------------------------------------------------------------------------------------

    protected RequestParams makeRequestParamsWith(String key, String value) {
        return new RequestParams(key, value);
    }

    protected RequestParams makeRequestParamsWith(Map<String, String> map) {
        return new RequestParams(map);
    }

}
