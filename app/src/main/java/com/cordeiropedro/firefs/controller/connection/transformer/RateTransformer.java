package com.cordeiropedro.firefs.controller.connection.transformer;

import com.cordeiropedro.firefs.controller.connection.listerners.RateListener;
import com.cordeiropedro.firefs.model.RequestModel;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class RateTransformer extends AbstractTransformer<RateListener> {

    public RateTransformer(RateListener listener) {
        super(listener);
    }

    @Override
    public void transform(JSONObject jsonObject, int statusCode, Header[] headers) {

        RequestModel requestModel = RequestModel.getInstance(jsonObject);
        listener.onSucess(requestModel, headers);

    }
}