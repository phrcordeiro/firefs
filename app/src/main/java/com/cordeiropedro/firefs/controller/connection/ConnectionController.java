package com.cordeiropedro.firefs.controller.connection;

import android.content.Context;

import com.cordeiropedro.firefs.controller.connection.transformer.AbstractTransformer;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.entity.StringEntity;

public class ConnectionController {


    protected static int TIME_OUT = 30000;
    protected AsyncHttpClient client;
    protected static ConnectionController INSTANCE;

    private ConnectionController() {
        if (client == null) {
            client = new AsyncHttpClient();
            client.setTimeout(TIME_OUT);
        }
    }

    public static ConnectionController getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ConnectionController();
        }
        return INSTANCE;
    }

    //----------------------------------------------------------------------------------------------
    // Puts
    //----------------------------------------------------------------------------------------------

    public void put(String url, JSONObject jsonObject, AbstractTransformer transformer, Context context) {
        try {
            StringEntity entity = new StringEntity(jsonObject.toString());
            client.put(context, url, entity, "application/json", transformer);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void put(Context context, String url, RequestParams params, AbstractTransformer transformer) {
        client.put(context, url, params, transformer);
    }

    //----------------------------------------------------------------------------------------------
    // Gets
    //----------------------------------------------------------------------------------------------

    public void get(Context context, String url, RequestParams params, AbstractTransformer transformer) {
        client.get(context, url, params, transformer);
    }

    public void get(Context context, String url, AbstractTransformer transformer) {
        client.get(context, url, transformer);
    }

    public void get(String url, AbstractTransformer transformer) {
        client.get(url, transformer);
    }

    //----------------------------------------------------------------------------------------------
    // Posts
    //----------------------------------------------------------------------------------------------

    public void post(String url, JSONObject jsonObject, AbstractTransformer transformer, Context context) {
        try {
            StringEntity entity = new StringEntity(jsonObject.toString());
            client.post(context, url, entity, "application/json", transformer);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void post(Context context, String url, RequestParams params, AbstractTransformer transformer) {
        client.post(context, url, params, transformer);
    }

    public void post(String url, AbstractTransformer transformer) {
        client.post(url, transformer);
    }

    //----------------------------------------------------------------------------------------------
    // Deletes
    //----------------------------------------------------------------------------------------------

    public void delete(Context context, String url, AbstractTransformer transformer) {
        client.delete(context, url, transformer);
    }

    //----------------------------------------------------------------------------------------------
    // Headers
    //----------------------------------------------------------------------------------------------

    public void addHeaderClient(String key, String value) {
        client.addHeader(key, value);
    }
}
