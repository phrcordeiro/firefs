------------------------------------------------------------------------------------------------------------------------------
Code
------------------------------------------------------------------------------------------------------------------------------

The code structure is based on MVC.

-------------------Controller package-----------------------------------------------------------------------------------------

Inside the controller package, you will find all classes for stablish a connection with API.

- ConnectionController has all request logic and settings for connection with API 

- AbstractRequester has all basic requester methods and attributes with any requester class will need.

- RateRequest has all the methods for rate request.
In this case the code only has only one class for request, but if was need get or/and edit some user data, for example, 
another class called UserRequest will be created with this methods.

- AbstractTransformer it receive the API response and makes the first validation like check the status code.

- RateTransformer it convert the json received by API to a RequestModel and send the object to active by some listener

- IListener define the basic methods for the listener

-------------------Model package----------------------------------------------------------------------------------------------

Inside the Model package, you will find all bussines logic and data model.

RequestModel - it represents return from API 

RateModel - it has all currency and methods for convert this currency values into a list

CurrencyModel - it has business logic for calculate the currency.

-------------------View package----------------------------------------------------------------------------------------------

Inside the View package, you will find all screen and screen logic.

AbstractMenuDrawerActivity - it has a class who contain all logic for create a menu drawer, 
if another activity need this menu, you can add this class inside the activity, 
the activity don't need extend this class making the activity free for extend any class.

FasterCardAdpter - this is a abstract class for card, any card list can extend that, 
and his soon only need implement the data card, it don't need be concerned about inflate the view and implement others methods.

------------------------------------------------------------------------------------------------------------------------------
Challenges 
------------------------------------------------------------------------------------------------------------------------------

The json structure isn't flexible.

If the rates was a vector and inside had a object with currency name and value, the code should be more simple 
when the code receive the json and convert to a object.

Following this logic the both methods into RateModel who return a collection won't be necessary and 
the application could use the request for filled the spinners, what makes the code more flexible 
because if some currency was added or removed, the code could update the spinners without change the android code.

------------------------------------------------------------------------------------------------------------------------------
Libraries 
------------------------------------------------------------------------------------------------------------------------------

Gson-> it makes the code simple and practical for convert json into a object.

ButterKife-> it makes the code more legible and remove all verbosity for create a activity and fragment.

AsyncHttp-> the connection for this application wasn't to complex, it didn't require a lot of things, 
then any famous library who execute the connection in background could be used in this programme, 
I choose it because I am more use to it and I spent a lot of time creating a good structure for this library.



